/*
** Zeta Network - App - ZCMS
** File : main.js
** Author: Robot
** Version : 0.2.0 - Beta
** USE STRICT REQUIRED
*/
"use strict";

var http = require('http'),
  WS = require(__dirname + "/src/ws.js"),
  DB = require(__dirname + '/src/db.js'),
  ARG = require(__dirname + '/src/arg.js'),
  LANGUAGE = require(__dirname + "/src/lang.js"),
  REQUEST = require(__dirname + '/src/request.js'),
  SETTINGS = require(__dirname + "/src/settings.js");

require('dotenv').config();

async function preStart() {
  console.log(`Hamac v${Hamac.version}`);

  // get argument
  ARG.init();

  // Start DB
  var r = await DB.start(ARG);
  if (!r.result) process.exit(75);

  // init settings
  await SETTINGS.init();

  // init language
  await LANGUAGE.init();
}

async function start() {
  await this.preStart();

  try {
    // create server
    this.server = http.createServer(REQUEST.request);

    // event register
    this.server.on('clientError', (err, socket) => {
      console.log(err);
      socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
    });

    // init websocket before server
    await WS.init(this.server);

    // start server
    this.server.listen(Hamac.port);

    this.postStart();
  } catch (e) {
    console.log(e);
    console.log("Cannot start, see error");
    process.exit(1);
  }
}

async function postStart() {
  console.log(`OK:\t\Hamac Running`);
  console.log("INFO:\t\tPort:\t"+Hamac.port);
}



class Hamac {

  static version = "0.1.0 - Beta";
  static port = process.env.HTTP_PORT;

  constructor() {
    this.server = null;

    this.start = start;
    this.preStart = preStart;
    this.postStart = postStart;
  }

}

new Hamac().start();

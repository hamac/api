var fs = require('fs'),
  DB = require(__dirname + "/db.js"),
  SETTINGS = require(__dirname + "/settings.js");

const PATH = __dirname + "/../lang/";
const REGEXP = /[$][{][{]((set.)?[a-zA-Z_0-9]+)[}][}]/gm;

/*
  INIT
*/

function init() {
  var files = fs.readdirSync(PATH);
  for (var i = 0; i < files.length; i++) {
    var file = files[i];
    if (file == ".gitkeep") continue;
    if (!file.endsWith(".json")) {
      console.log(`Invalid language file: ${file}, skipping`);
      continue;
    }
    loadFile(file);
  }

  console.log("Default language: "+process.env.APP_DEFAULT_LANG);
  module.exports.default = "en"; //process.env.APP_DEFAULT_LANG;
}

function loadFile(file) {
  var name = file.split(".")[0];
  console.log(`Loading language: ${name}`);
  try {
    var data = fs.readFileSync(PATH + file);
    var json = JSON.parse(data);
    module.exports[name] = json;
  } catch (e) {
    console.log(e);
    console.log("Error loading language");
    process.exit(10);
  }
}

/*
  RUN
*/
function getLang(lang) {
  return module.exports.hasOwnProperty(lang) ? lang : module.exports.default;
}

async function run(req, res, path) {
  var html = fs.readFileSync(path, 'utf-8');
  return await runString(req, res, html)
}

async function runString(req, res, html) {
  var lang = getLang(req.lang);
  var ids = [];

  html = html.replace("$[[]]", "");
  for (var m = null; m = REGEXP.exec(html); ids.push(m[1]));

  for (var i = 0; i < ids.length; i++) {
    console.log(i, ids[i]);
    var text = `LangError: ${ids[i]}`;
    if (module.exports[lang].hasOwnProperty(ids[i])) text = module.exports[lang][ids[i]];
    if (ids[i] == "ALL_LANG") text = JSON.stringify(module.exports[lang]);
    if (ids[i].startsWith("set.")) text = SETTINGS[ids[i].split(".")[1]];
    html = html.replace("${{"+ids[i]+"}}", text);
  }

  send(req, res, html);
  return true;
}

function send(req, res, html) {
  try {
    res.writeHead(200, {"Content-Type":"text/html", "Content-Length": html.length});
    res.write(html);
    res.end();
  } catch (e) {
    console.log(e);
    res.writeHead(400).write('Error sending resource');
  }
}

module.exports = {
  init: init,
  run: run,
  runString: runString
}

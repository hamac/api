"use strict";

const fs = require('fs');

var DB = require(__dirname + "/db.js"),
  ARG = require(__dirname + "/arg.js"),
  ADMIN = require(__dirname + "/admin.js"),
  LANGUAGE = require(__dirname + "/lang.js");

const REGEXP = /[$][{]{data.([a-zA-Z_0-9]+)[}][}]/gm;

async function render(req, res) {
  if (res == null) return ({result:false, info:"No res object"});

  var logged = await checkUser(req, res);
  if (!logged.result) return sendLogin(req, res, logged);

  var html = fs.readFileSync(ARG.path.dashboard, 'utf-8');
  html = pushUserData(html, logged.admin);
  LANGUAGE.runString(req, res, html);
  return ({exit:true});
}

async function checkUser(req, res) {
  if (!req.body.hasOwnProperty('email') || !req.body.hasOwnProperty('password')) return ({result:false, info:"Missing Credential", langKey:"missing_credential"});

  var user = await ADMIN.login(req.body.email, req.body.password);
  if (!user.result) return (user);

  return ({result:true, admin:user.admin});
}

function sendLogin(req, res, r) {
  var html = fs.readFileSync(ARG.path.login, 'utf-8');
  html = html.replace("$[[]]", "${{"+r.langKey+"}}");
  LANGUAGE.runString(req, res, html);
  return ({exit:true});
}

function pushUserData(html, user) {
  // push user
  delete user._id;
  delete user.password;
  html = html.replace("${{RAW_USER}}", JSON.stringify(user));

  // push js link
  var jsLink = `<script src="${module.exports.js.prod}" charset="utf-8"></script>`;
  if (ARG.debug) {
    jsLink = "";
    for (var i = 0; i < module.exports.js.debug.length; i++) {
      jsLink += `<script src="${module.exports.js.debug[i]}" charset="utf-8"></script>`;
    }
  }
  html = html.replace("${{JS}}", jsLink);

  return html;
}

/*
  Set Selection
*/
async function setUserDashboard(req, res) {
  if (!req.body.hasOwnProperty('dashboard') || !Array(req.body.dashboard)) return ({result:false, info:"Missing dashboard list"});

  // get available
  var available = await getAvailable();
  if (!available.result) return (available);

  // get actual user dashboard
  var dash = await getUserDashboard(req.user.userId);
  if (!dash.result) return (dash);

  // compute
  var newList = [], notFound = [];
  for (var i = 0; i < req.body.dashboard.length; i++) {
    var key = req.body.dashboard[i];
    var found = false;

    for (var i = 0; i < available.available.length; i++) {
      var item = available.available[i];
      if (item.key == key) {
        found = true;
        newList.push(item);
        break;
      }
    }
    if (!found) notFound.push(key);
  }

  // update
  dash.data.items = newList;
  var update = await DB.updateOne(DB.dashboard, dash.data);
  if (!update.result) return (update);

  return ({result:true, info:"Dashboard Updated"});
}

/*
  Get
*/
async function getUserDashboard(userId, create=true) {
  var found = await DB.findOne(DB.dashboard, {userId:userId});
  if (found.hasOwnProperty("errorCode") && found.errorCode == 1756) {
    // create
    var newDashboard = {userId: userId, items:[]};
    var insert = await DB.insertOne(DB.dashboard, newDashboard);
    if (!insert.result) return (insert);
    return await getUserDashboard(userId, false);
  } else {
    return (found);
  }
}

async function getAvailable(req, res) {
  var found = await DB.findAll(DB.dashboard_available, true);
  if (!found.result) return (found);
  return ({result:true, available: found.data});
}

/*
  Build
*/
async function build(req, res) {
  // get user dashboard
  var dash = await getUserDashboard(req.user.userId);
  if (!dash.result) return (dash);

  // build
  var list = dash.data.items, dashboard = [];
  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var r = await BUILDER.build(item);
    if (!r.result) return (r);
    dashboard.push(r.item);
  }

  return ({result:true, dashboard:dashboard});
}

/*
  Map
*/
async function map(req, res) {
  if (req.action.length == 1) return await run(req, res);

  switch (req.action[1]) {
    case "my": return await build(req, res);
    case "setSelection": return await setUserDashboard(req, res);
    case "available": return await getAvailable(req, res);
    default: return ({result:false, info:`Not found under 'dashboard': ${req.action[1]}`, errorCode: 404});
  }
}

module.exports = {

  js: {
    debug: [
      "/js/lib/apps.js"
    ],
    prod: "/zeta/js/zcms.min.js",
  },

  render: render,
  map: map
}

"use strict";

var fs = require('fs'),
  url = require('url'),
  querystring = require('querystring'),
  formidable = require('formidable'),
  ARG = require(__dirname + "/arg.js"),
  TOOLS = require(__dirname + "/tools.js"),
  MAPPER = require(__dirname + "/mapper.js"),
  LANGUAGE = require(__dirname + "/lang.js");

async function endOfRequest(req, res) {
  // parse multipart if needed
  if (req.headers.hasOwnProperty('content-type') && req.headers["content-type"].startsWith("multipart/form-data;")) {
    return await parseMultipart(req, res);
  }

  // Just wait the end of the request
  return new Promise((resolve, reject) => {
    var body = "";
    req.on('data', chunk => {body += chunk;});
    req.on('end', () => {
      // parse requets
      var r = parseBody(req, res, body);
      // invalid body
      if (!r.result) return;
      // execute request
      req.body = r.body;
      start(req, res);
    });
  });
}

function parseBody(req, res, body) {
  // check if have content type header
  if (!req.headers.hasOwnProperty('content-type')) return ({result:true, body:{}});
  var type = req.headers['content-type'];

  console.log("type", type, ", method", req.method);

  // try arsing body
  try {
    switch (type) {
      case "application/json":
        return ({result:true, body:JSON.parse(body)});
      case "application/x-www-form-urlencoded":
        return ({result:true, body:Object.assign({}, querystring.decode(body)), base:body});
      default:
        console.log("Unknwon type:", type);
        return ({result:true, body:{}});
    }
  } catch (e) {
    res.setHeader('Content-Type', 'application/json');
    res.writeHead(400);
    res.write(JSON.stringify({result:false, info:"Error reading your body: Invalid JSON body"}));
    res.end();
    return ({result:false, error:e});
  }
}

async function parseMultipart(req, res) {
  return new Promise((resolve, reject) => {

    const form = formidable({
      uploadDir: __dirname + "/../uploads/tmp",
      keepExtensions: true,
      multiples: true,
      filename(name, ext, part, form) {
        var d = new Date();
        console.log("name", name, "ext", ext);
        return `${d.getFullYear()}_${d.getMonth()}_${d.getDate()}__${name.trim().replace(" ", "")}${ext}`;
      }
    });

    form.parse(req, (err, fields, files) => {
      if (err) {
        console.log(err);
        res.setHeader('Content-Type', 'application/json');
        res.writeHead(400);
        res.write(JSON.stringify({result:false, info:"Error parsing body"}));
        res.end();
        return;
      }

      req.body = fields;
      req.files = files;
      start(req, res);
    });

  });
}

/*
  Pulic Part
*/

async function isPublic(req, res, query) {
  //console.log(req.method, req.url);
  if (req.method != "GET") return false;

  var path = __dirname + '/../public' + req.query.pathname;
  if (req.query.pathname.startsWith("/uploads/")) path = __dirname + "/.." + req.query.pathname;
  if (req.query.pathname == "/favicon.ico") path = ARG.path.favicon;
  if (req.query.pathname == "/") path = ARG.path.login;

  // dont push html that are directly asked

  return await verifyPublic(req, res, path);
}

async function verifyPublic(req, res, path) {
  // check if file exist
  var exist = await TOOLS.isPublicExist(path);
  if (!exist) return false;

  // if html file return via language
  if (path.split(".").pop() == "html") return LANGUAGE.run(req, res, path);

  // file exist, send it
  return TOOLS.sendPublic(req, res, path);
}

/*
  Compute
*/

async function start(req, res) {
  // run firewall
  //if (!FIREWALL.verify(req, res)) return;

  // parse url & body language
  req.query = url.parse(req.url, true);
  req.query.query = JSON.parse(JSON.stringify(req.query.query));
  req.lang = parseLanguage(req);

  // public verification
  var used = await isPublic(req, res);
  if (used == true) return;

  req.action = req.query.path.split('/');
  if (req.action[0] == "") req.action.splice(0, 1);

  // handle the page
  await MAPPER.fromRequest(req, res);
}

function parseLanguage(req) {
  if (req.body.hasOwnProperty('lang')) return req.body.lang;
  if (req.body.hasOwnProperty('language')) return req.body.language;
  if (req.query.query.hasOwnProperty('lang')) return req.query.query.lang;
  if (req.query.query.hasOwnProperty('language')) return req.query.query.language;
  return LANGUAGE.default;
}

module.exports = {
  request: endOfRequest
}

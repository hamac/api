/*
** Hamac - DB
** DB functions
** Author: Robot
** Version : 1.1.0
** Mongo Version : ^3.3.5
*/
"use strict";

var MONGODB = require('mongodb');

const INT_MIN = 100000000, INT_MAX = 999999999;

/*
  Initialization
*/
async function start(arg) {

  var tmp = verifyConfig(arg);
  if (!tmp.result) return(tmp);

  var r = await connect(tmp);
  if (!r.result) return(r);
  if (!arg.gitlabCI) return(r);

  return ({result:true});
}

async function connect(config, dbName) {
  const MongoClient = MONGODB.MongoClient;

  console.log(`Connecting to DB: ${config.safeUrl}...`);
  const client = new MongoClient(config.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
   });

  try {
    await client.connect();

    module.exports.url = config.url;
    module.exports.client = client;
    module.exports.db = client.db(config.dbName);
    module.exports.host = config.host;
    module.exports.port = config.port;

    console.log("Connected to DB");
    return({result:true, info:"Connected to DB"});
  } catch (err) {
    console.log(err);
    return({result:false, info:"Error connecting to DB, see error.", error:err, errorCode: 1750});
  }
}

function verifyConfig(arg) {
  var host = process.env.DB_HOST;
  var port = process.env.DB_PORT;
  var cred = process.env.DB_USER + ":" + process.env.DB_PASSWORD + "@";
  var db = process.env.DB_NAME;

  if (host == "") {
    return ({result:false, info:"Host cannot be empty"});
  } else if (db == "") {
    return ({result:false, info:"DB can't be empty"});
  }

  if (arg.gitlabCI === true) {
    host = "mongo";
  }

  if (process.env.DB_USER == "" || process.env.DB_PASSWORD == "") {
    console.log("Empty DB credential, connect without login");
    cred = "";
  }

  const url = `mongodb://${cred}${host}:${port}/${db}`;
  const safeUrl = `mongodb://${host}:${port}/${db}`;
  return ({result:true, url:url, db:db, safeUrl:safeUrl, host:host, port:port});
}

/*
  Find part
*/

async function findOne(coll, toFind) {
  const collection = module.exports.db.collection(coll);

  try {
    var data = await collection.findOne(toFind);

    if (data === null) {
      return ({result:false, info:"Data not found", errorCode: 1756});
    }

    return ({result:true, data:data});
  } catch (e) {
    console.log(e);
    return ({result:false, info:`Error get in collection, see error.`, error:e, errorCode: 1752});
  }
}

async function findAll(coll, outside=false) {
  const collection = module.exports.db.collection(coll);

  try {
    var data = await collection.find({}).toArray();

    if (!Array.isArray(data)) {
      data = [data];
    }

    if (outside) {
      for (var i = 0; i < data.length; i++) {
        delete data[i]._id;
      }
    }

    return ({result:true, data:data});
  } catch (e) {
    console.log(e);
    return ({result:false, info:`Error get in collection, see error.`, error:e, errorCode: 1752});
  }
}

async function findMultiple(coll, toFind, sort={}, limit=0, outside=false, projection={}) {
  const collection = module.exports.db.collection(coll);

  try {
    var docs = await collection.find(toFind, projection).limit(limit).sort(sort).toArray();

    if (!Array.isArray(docs)) {
      docs = [docs];
    }

    if (outside) {
      for (var i = 0; i < docs.length; i++) {
        delete docs[i]._id;
      }
    }

    return({result:true, data:docs});
  } catch (e) {
    console.log(e);
    return({result:false, info:`Error get in collection, see error.`, error:e, errorCode: 1752});
  }
}

/*
  Insert
*/

async function insertOne(coll, data) {
  const collection = module.exports.db.collection(coll);

  try {
    var r = await collection.insertOne(data);
    return ({result:true, data_id: r.insertedId});
  } catch (e) {
    console.log(e);
    return ({result:false, info:`Error insert collection, see error.`, error:e, errorCode: 1751});
  }
}


/*
  Update
*/

async function updateOne(coll, data) {
  var ObjectID = MONGODB.ObjectID;
  const collection = module.exports.db.collection(coll);

  if (!data.hasOwnProperty('_id')) {
    return ({result:false, info:"Cannot update data without _id", errorCode: 1757});
  }

  try {
    var result = await collection.updateOne({_id: new ObjectID(data._id)}, {$set: data});
    return({result:true, info:"Data updated."});
  } catch (e) {
    console.log(e);
    return({result:false, info:`Error update in collection, see error.`, error:e, errorCode: 1753});
  }
}

async function updateOneQuery(coll, find, update) {
  const collection = module.exports.db.collection(coll);

  try {
    var result = await collection.updateOne(find, update);
    return({result:true, info:"Data updated."});
  } catch (e) {
    console.log(e);
    return({result:false, info:`Error update in collection, see error.`, error:e, errorCode: 1753});
  }
}

async function updateMultiple(coll, list) {
  try {
    var ok = true, error = [];

    for (var i = 0; i < list.length; i++) {
      var result = await updateOne(coll, list[i]);
      if (!result.result) {
        ok = false;
        error.push(result.info + "||" + result.error);
      }
    }

    return({result:ok, info: error.length == 0 ? "Data updated." : "Error updating some document", error:error});
  } catch (e) {
    console.log(e);
    return({result:false, info:`Error update in collection, see error.`, error:e, errorCode: 1753});
  }
}

/*
  Delete
*/

async function deleteOne(coll, toFind=null, data_id=null) {
  var ObjectID = MONGODB.ObjectID;
  const collection = module.exports.db.collection(coll);

  if (data_id !== null) {
    toFind = {_id: new ObjectID(data_id)};
  }

  try {
    var result = await collection.deleteOne(toFind);

    return({result:true, info:"Data removed."});
  } catch (e) {
    console.log(e);
    return ({result:false, info:`Error delete in 'test', see error.`, error:e, errorCode: 1754});
  }
}

async function deleteMultiple(coll, toFind) {
  const collection = module.exports.db.collection(coll);

  try {
    var result = await collection.deleteMany(toFind);

    console.log(result.deletedCount);
    return({result:true, info:"Data removed.", deletedCount:result.deletedCount});
  } catch (e) {
    console.log(e);
    return({result:false, info:`Error delete in collection, see error.`, error:err, errorCode: 1754});
  }
}

async function dropCollection(coll) {
  const collection = module.exports.db.collection(coll);

  try {

    var result = await collection.drop();

    if (!result.result) {
      return ({result:false, dropStatus:result, info:"Internal Error", errorCode: 1759});
    }

    return({result:true, info:"Collection droppped"});
  } catch (e) {
    console.log(e);
    return({result:false, info:`Error drop collection, see error.`, error:e, errorCode: 1755});
  }
}

/*
  ID generation
*/

async function generateId(size, coll=null, field=null) {
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var id = "";

  for (var i = 0; i < size; i++) {
    if (i % 12 == 0 && i != 0) id += "-";
    id += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  if (coll != null && field != null) return await verifyGeneratedId(size, id, coll, field);
  return id;
}

async function verifyGeneratedId(size, id, coll, field) {
  const toFind = {};
  toFind[field] = id;

  var found = await findOne(coll, toFind);
  if (!found.result) return (id);

  return generateId(size, coll, field);
}

function randomNumber(min=INT_MIN, max=INT_MAX) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min +1)) + min;
}

/*
  Module
*/

module.exports = {

  url: "",
  client: null,
  db: null,

  objectId: MONGODB.ObjectID,

  // Collections
  admin: "admin",
  settings: "settings",

  start: start,
  connect: connect,
  verifyConfig: verifyConfig,

  generateId: generateId,
  randomNumber: randomNumber,

  /*
  * Custom Insert Part
  */

  insertOne: insertOne,

  customInsertMultiple: async function (coll, list) {
    const collection = module.exports.db.collection(coll);

    try {
      var r = await collection.insert(list);

      if (r.result.ok !== list.length) {
        return ({result:false, info:"Internal Error", errorCode: 1759});
      }

      return ({result:true, info:`${list.length} data pushed.`});
    } catch (e) {
      console.log(e);
      return ({result:false, info:`Error insert in collection, see error.`, error:err, errorCode: 1751});
    }
  },

  /*
  * Custom Find Part
  */

  findOne: findOne,
  findAll: findAll,
  findMultiple: findMultiple,

  /*
  * Custom Random
  */

  customRandom: async function (coll, size, outside=false) {
    const collection = module.exports.db.collection(coll);
    try {
      var data = await collection.aggregate([{ $sample: { size: size } }]).toArray();

      if (!Array.isArray(data)) {
        data = [data];
      }

      if (outside) {
        for (var i = 0; i < data.length; i++) {
          delete data[i]._id;
        }
      }

      return ({result:true, data:data});
    } catch (e) {
      console.log(e);
      return ({result:false, info:`Error get in collection, see error.`, error:e, errorCode: 1752});
    }
  },

  /*
    Update
  */

  updateOne: updateOne,
  updateOneQuery: updateOneQuery,
  updateMultiple: updateMultiple,

  /*
    Delete
  */

  deleteOne: deleteOne,
  deleteMultiple: deleteMultiple,

  dropCollection: dropCollection,

  /*
  * Custom Part
  * End
  */

  getActualHost: function () {
    console.log("HOST", module.exports.host);
    return module.exports.host;
  },

  getActualPort: function () {
    console.log("PORT", module.exports.port);
    return module.exports.port;
  }

}

"use strict";

/*
  GET
*/
function get(userId) {
  for (var i = 0; i < module.exports.clients.length; i++) {
    if (module.exports.clients[i].user.userId == userId) return module.exports.clients[i];
  }
  return null;
}

/*
  SEND
*/
function send(userId, data) {
  if (userId == "all") return sendAll(data);
  var client = get(userId);
  if (client == null) return;

  client.send(typeof data == "string" ? data : JSON.stringify(data));
}

function sendAll(data) {
  for (var i = 0; i < module.exports.clients.length; i++) {
    var client = module.exports.clients[i];
    if (client.auth == false  || client.lock == true) continue;
    client.send(typeof data == "string" ? data : JSON.stringify(data));
  }
}

/*
  LOCK
*/
function lockIfConnected(userId) {
  var client = get(userId);
  if (client == null) return console.log("Cannot lock", userId, "not connected");

  client.locked = true;
  client.send("lock");
}

/*
  OnClose
*/
function onClose(client) {
  if (!client.hasOwnProperty('user')) return console.log('Client was not added');

  for (var i = 0; i < module.exports.clients.length; i++) {
    if (module.exports.clients[i].user.userId != client.user.userId) continue;
    module.exports.clients.splice(i, 1);
    //console.log("OnClose: found removed", module.exports.clients.length);
    return;
  }
  //console.log("OnClose: not found", client.user);
}


module.exports = {

  clients: [],

  send: send,
  onClose: onClose,
  lockIfConnected: lockIfConnected
}

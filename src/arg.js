const INTS = [
  "DB_PORT", "HTTP_PORT"
],
BOOLEANS = [];

function init() {
  console.log("FAVICON", module.exports.path.favicon);
  try {
    Object.assign(module.exports, process.env);

    for (var i = 0; i < INTS.length; i++) {
      module.exports[INTS[i]] = parseInt(module.exports[INTS[i]]);
    }

    for (var i = 0; i < BOOLEANS.length; i++) {
      module.exports[BOOLEANS[i]] = module.exports[BOOLEANS[i]] == "true";
    }

    checkArg();
  } catch (e) {
    console.log(e);
    process.exit(2);
  }
}

function checkArg() {
  var argv = process.argv;

  module.exports.path.node = argv[0];
  module.exports.path.exec = argv[1];

  for (var i = 2; i < argv.length; i++) {
    switch (argv[i]) {
      case "--debug":
        module.exports.debug = true;
        break;
      default:
        console.log("Unknown argument:", argv[i]);
    }
  }
}

module.exports = {
  init: init,

  debug: false,
  gitlabCI: false,
  path: {
    favicon: __dirname + "/../public/" + process.env.APP_FAVICON,
    dashboard: __dirname + "/../public/dashboard.html",
    login: __dirname + "/../public/login.html"
  },
};

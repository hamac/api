/*
** Zeta Network - Hamac - WebSocket
** File : ws.js
** Description : WebSocket Server
** Author: Robot
** Version : 2.0.0 - Beta
** USE STRICT REQUIRED
*/
"use strict";

var { WebSocketServer } = require('ws');

var DB = require(__dirname + "/db.js"),
  ADMIN = require(__dirname + "/admin.js"),
  MAPPER = require(__dirname + "/mapper.js"),
  CLIENTS = require(__dirname + "/clients.js"),
  LANGUAGE = require(__dirname + "/lang.js");

const ABSENT = "absent",
  ONLINE = "online",
  OFFLINE = "offline";

async function init(server) {
  const wss = new WebSocketServer({ server });

  /*server.on('upgrade', function (request, socket, header) {});*/

  wss.on('connection', (client) => {
    if (client.protocol != "marina-ws") {
      client.close(1002, "Invalid protocol"); // 1002 -> "Protocol error"
      return;
    }

    client.on('message', (data) => { onMessage(client, String(data)) });
    client.on('close', () => { CLIENTS.onClose(client); });

    client.authenticated = false;
    client.authTry = 0;
    client.send("auth");
  });

  console.log("WebSocket server started");
  setInterval(checkUserStatus, 1000 * 60);
  return ({result:true, info:"WebSocket server started"});
}

/*
  Message
*/

async function onMessage(client, msg) {
  if (client.authenticated == false || client.user == null || client.locked == true) {
    return verifyClient(client, msg);
  }

  if (msg == "lock") return lock(client);

  try {
    map(client, JSON.parse(msg));
  } catch (e) {
    console.log(e);
    client.send(JSON.stringify({result:false, info:"Error", error:e}));
    return;
  }
}

async function verifyClient(client, msg) {
  if (client.authTry >= 3) {
    client.close(3000, "Authentication try exceeded"); // 3000 -> Unauthorized
    return;
  } else if (!msg.startsWith('auth') && !msg.startsWith('unlock')) {
    if (client.authenticated == false) client.send("auth");
    else if (client.locked == true) client.send("need-unlock");
    return;
  }

  var cred = msg.split(":");
  if (cred[0] == "unlock") {
    unlock(client, cred)
  } else if (cred[0] == "auth") {
    loginClient(client, cred);
  } else {
    client.send("auth");
  }
}

/*
  Login
*/

async function loginClient(client, cred) {
  if (cred.length != 4) {
    client.authTry++;
    client.send("auth");
    return;
  }

  var login = await ADMIN.viaToken(cred[1], cred[2], cred[3]);
  if (!login.result) {
    client.authTry++;
    client.send("auth");
    return;
  }

  client.authenticated = true;
  client.locked = false;
  client.user = login.admin;
  client.authTry = 0;
  client.callback = [];
  CLIENTS.clients.push(client);
  client.send("authenticated");
  setStatus(client.user.userId, ONLINE);
}

/*
  Lock
*/
async function unlock(client, cred) {
  if (cred.length != 2) {
    client.send("need-unlock");
    return;
  }

  var check = await ADMIN.checkPassword(client.user.userId, cred[1]);
  console.log("unlock", check);
  if (!check.result) {
    client.send("unlock-failed");
  } else {
    client.send("unlocked");
    client.locked = false;
    setStatus(client.user.userId, ONLINE);
  }
}

function lock(client) {
  setStatus(client.user.userId, ABSENT);
  client.locked = true;
  return;
}

/*
  Satus
*/
async function checkUserStatus() {
  var list = await DB.findMultiple(DB.admin, {
    lastActivity: { $lt: new Date(new Date().getTime() - (1000 * 60 * 5)) },
    status: { $ne: "offline" }
  });

  if (!list.result) {
    console.log(list);
    return;
  } else if (list.data.length == 0) {
    return;
  }

  var absent = new Date(new Date().getTime() - (1000 * 60 * 5)); // 5m
  var offline = new Date(new Date().getTime() - (1000 * 60 * 10)); // 10m
  for (var i = 0; i < list.data.length; i++) {
    var last = new Date(list.data[i].lastActivity);

    if (last.getTime() <= offline.getTime()) {
      setStatus(list.data[i].userId, OFFLINE);
      CLIENTS.lockIfConnected(list.data[i].userId);
    } else if (last.getTime() <= absent.getTime()) {
      setStatus(list.data[i].userId, ABSENT);
      CLIENTS.lockIfConnected(list.data[i].userId);
    }
  }
}

async function setStatus(userId, status) {
  var toSet = { status: status };
  if (status == ONLINE) toSet.lastActivity = new Date();

  var update = await DB.updateOneQuery(DB.admin, {userId: userId}, { $set: toSet});
  if (!update.result) console.log(userId, update);
}

/*
  MAP
*/

async function map(client, data) {
  setStatus(client.user.userId, ONLINE);
  if (!data.hasOwnProperty('action')) {
    client.send(JSON.stringify({result:false, info:"Missing action"}));
    return;
  }
  var action = data.action.split("/");
  if (action[0] == "") action.shift();
  var response = await MAPPER.map({
    action: action,
    body: data.hasOwnProperty('data') ? data.data : {},
    lang: data.hasOwnProperty('lang') ? data.lang : LANGUAGE.default,
    user: client.user
  }, null);
  if (data.hasOwnProperty('tmpId')) response.tmpId = data.tmpId;
  response.fromAction = data.action;
  client.send(JSON.stringify(response));
}

module.exports = {

  server: null,
  clients: [],

  timeout: {
    afk: 1,
    offline: 1
  },

  init: init

}

"use strict";

var fs = require('fs'),
  path = require('path');

const PATH_TMP = __dirname + "/../uploads/tmp";
const PATH_UPLOAD = __dirname + "/../uploads";

var BASE = path.normalize(__dirname + '/..');

function fromRequest(req, res) {
  //console.log("body", req.body, "files", req.files);
  var upload = {};
  var keys = Object.keys(req.files);

  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    upload[key] = [];
    if (!Array.isArray(req.files[key])) req.files[key] = [req.files[key]];

    for (var j = 0; j < req.files[key].length; j++) {
      upload[key].push(req.files[key][j].filepath.replace(BASE, ""));
    }
  }

  return ({result:true, files:upload});
}

/*
  VALID
*/

function validTempFile(tmpFile, newName) {
  try {
    if (!tmpFile.startsWith("/uploads/tmp")) return ({result:false, info:"Not a temp file"});

    var tmpFile = BASE + tmpFile;
    if (!fs.existsSync(tmpFile)) return ({result:false, info:"Temp file does not exist"});

    var ext = tmpFile.split('.').pop();
    var newName = newName + "." + ext;

    //console.log(tmpFile, "->", PATH_UPLOAD + "/" + newName);
    fs.renameSync(tmpFile, PATH_UPLOAD + "/" + newName);

    return ({result:true, name:"/uploads/"+newName});
  } catch (e) {
    console.log(e);
    return ({result:false, info:"Internal error processing file", error:e});
  }
}

/*
  Remove
*/
function checkForRemovedFile(previous, next, key="") {
  console.log(previous, "->", next);
  if (next.length == 0) return removeAllFile(previous, key);

  if (typeof next[0] == "object") {
    previous = toStringList(previous, key);
    next = toStringList(next, key);
  }

  for (var i = 0; i < previous.length; i++) {
    var name = previous[i];
    if (next.includes(name)) continue;

    console.log("clear", name);
    var r = removeFile(name);
    if (!r.result) console.log(r);
  }
}

function removeAllFile(list, key="") {
  var result = {result:true, skipped:[]};

  for (var i = 0; i < list.length; i++) {
    var name = list[i];

    if (typeof list[i] == "object") {
      if (key == "") continue;
      name = list[i][key];
    }

    var r = removeFile(name);
    if (!r.result) {
      result.result = false;
      result.skipped.push(name);
      console.log(r);
    }
  }
  return (result);
}

function removeFile(name) {
  try {
    if (!name.startsWith("/uploads")) return ({result:false, info:"Not an upload file"});
    fs.rmSync(BASE + name);
    return ({result:true, info:"File Deleted"});
  } catch (e) {
    console.log(e);
    return ({result:false, info:"Internal error processing file", error:e});
  }
}

/*
  Tool
*/
function toStringList(list, key) {
  var done = [];
  for (var i = 0; i < list.length; i++) {
    if (!list[i].hasOwnProperty(key)) continue;
    done.push(list[i][key]);
  }
  return done;
}


module.exports = {
  fromRequest: fromRequest,
  validTempFile: validTempFile,

  removeFile: removeFile,
  removeAllFile: removeAllFile,
  checkForRemovedFile: checkForRemovedFile,
}

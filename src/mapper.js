"use strict";

const fs = require('fs');

var TOOLS = require(__dirname + "/tools.js"),
  ADMIN = require(__dirname + "/admin.js"),
  DASHBOARD = require(__dirname + "/dashboard.js");

const NOT_FOUND = {result:false, info:"Not Found", errorCode:404};

async function fromRequest(req, res) {

  if (req.query.pathname == "/dashboard") {
    DASHBOARD.render(req, res);
    return;
  }
  /*if (req.query.pathname.startsWith("/zcms")) {
    var logged = await ADMIN.verifyHeader(req);
    if (!logged.result) {
      res.setHeader('Content-Type', 'application/json');
      res.writeHead(200);
      res.end(JSON.stringify(logged));
      return;
    }
    req.user = logged.admin;
  }*/

  var body = await map(req, res);
  if (body.exit == true) return;
  //console.log("RETURN", body);
  res.setHeader('Content-Type', 'application/json');
  res.writeHead(200);
  res.end(JSON.stringify(body));
}

async function map(req, res=null) {
  var body = Object.assign({}, NOT_FOUND);
  switch (req.action[0]) {
    default: return body;
  }
}

module.exports = {
  openPaths: [
    "/dashboard"
  ],

  fromRequest: fromRequest,
  map: map
}

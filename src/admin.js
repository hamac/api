"use strict";

var DB = require(__dirname + "/db.js"),
  CRYPTO = require(__dirname + "/crypto.js");

const INVALID = {result:false, info:"Invalid Credential", langKey:"invalid_credential"};

async function login(email, password) {
  var found = await getByEmail(email);
  if (!found.result) return (INVALID);

  var good = await CRYPTO.compare(password, found.admin.password);
  if (!good.result) return (INVALID);

  return ({result:true, admin:found.admin});
}

async function viaToken(email, token, secret) {
  var found = await getByEmail(email);
  if (!found.result) return (INVALID);
  if (found.admin.token != token) return (INVALID);
  if (found.admin.secret != secret) return (INVALID);

  return ({result:true, admin:found.admin});
}

async function verifyHeader(req) {
  if (!req.headers.hasOwnProperty('x-zcms-email')
    || !req.headers.hasOwnProperty('x-zcms-token')
    || !req.headers.hasOwnProperty('x-zcms-secret')) return (INVALID);

  var found = await getByEmail(req.headers["x-zcms-email"]);
  if (!found.result) return (INVALID);
  if (found.admin.token != req.headers["x-zcms-token"]) return (INVALID);
  if (found.admin.secret != req.headers["x-zcms-secret"]) return (INVALID);

  return ({result:true, admin:found.admin});
}

async function checkPassword(userId, passwd) {
  var found = await getById(userId);
  if (!found.result) return (INVALID);

  var good = await CRYPTO.compare(passwd, found.admin.password);
  if (!good.result) return (INVALID);

  return ({result:true, info:"Password Match"});
}

/*
  GET
*/

async function getById(userId) {
  var found = await DB.findOne(DB.admin, {userId: userId});
  if (!found.result) return ({result:false, info:"Admin not found"});
  return ({result:true, admin:found.data});
}

async function getByEmail(email) {
  var found = await DB.findOne(DB.admin, {email: email});
  if (!found.result) return ({result:false, info:"Admin not found"});
  return ({result:true, admin:found.data});
}

module.exports = {
  login: login,
  viaToken: viaToken,
  verifyHeader: verifyHeader,
  checkPassword: checkPassword
}

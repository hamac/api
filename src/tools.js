var fs = require('fs');

const TYPES = [
  { ext: "aac", type: "audio/aac"},
  { ext: "abw", type: "application/x-abiword"},
  { ext: "arc", type: "application/octet-stream"},
  { ext: "avi", type: "video/x-msvideo"},
  { ext: "azw", type: "application/vnd.amazon.ebook"},
  { ext: "bin", type: "application/octet-stream"},
  { ext: "bmp", type: "image/bmp"},
  { ext: "bz", type: "application/x-bzip"},
  { ext: "bz2", type: "application/x-bzip2"},
  { ext: "csh", type: "application/x-csh"},
  { ext: "css", type: "text/css"},
  { ext: "csv", type: "text/csv"},
  { ext: "doc", type: "application/msword"},
  { ext: "docx", type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
  { ext: "eot", type: "application/vnd.ms-fontobject"},
  { ext: "epub", type: "application/epub+zip"},
  { ext: "gif", type: "image/gif"},
  { ext: ["htm", "html"], type: "text/html"},
  { ext: "ico", type: "image/x-icon"},
  { ext: "ics", type: "text/calendar"},
  { ext: "jar", type: "application/java-archive"},
  { ext: ["jpeg","jpg"], type: "image/jpeg"},
  { ext: "js", type: "application/javascript"},
  { ext: "json", type: "application/json"},
  { ext: ["mid", "midi"], type: "audio/midi"},
  { ext: "mpeg", type: "video/mpeg"},
  { ext: "mpkg", type: "application/vnd.apple.installer+xml"},
  { ext: "odp", type: "application/vnd.oasis.opendocument.presentation"},
  { ext: "ods", type: "application/vnd.oasis.opendocument.spreadsheet"},
  { ext: "odt", type: "application/vnd.oasis.opendocument.text"},
  { ext: "oga", type: "audio/ogg"},
  { ext: "ogv", type: "video/ogg"},
  { ext: "ogx", type: "application/ogg"},
  { ext: "otf", type: "font/otf"},
  { ext: "png", type: "image/png"},
  { ext: "pdf", type: "application/pdf"},
  { ext: "ppt", type: "application/vnd.ms-powerpoint"},
  { ext: "pptx", type: "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
  { ext: "rar", type: "application/x-rar-compressed"},
  { ext: "rtf", type: "application/rtf"},
  { ext: "sh", type: "application/x-sh"},
  { ext: "svg", type: "image/svg+xml"},
  { ext: "swf", type: "application/x-shockwave-flash"},
  { ext: "tar", type: "application/x-tar"},
  { ext: ["tif","tiff"], type: "image/tiff"},
  { ext: "ts", type: "application/typescript"},
  { ext: "ttf", type: "font/ttf"},
  { ext: "vsd", type: "application/vnd.visio"},
  { ext: "wav", type: "audio/x-wav"},
  { ext: "weba", type: "audio/webm"},
  { ext: "webm", type: "video/webm"},
  { ext: "webp", type: "image/webp"},
  { ext: "woff", type: "font/woff"},
  { ext: "woff2", type: "font/woff2"},
  { ext: "xhtml", type: "application/xhtml+xml"},
  { ext: "xls", type: "application/vnd.ms-excel"},
  { ext: "xlsx", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
  { ext: "xml", type: "application/xml" },
  { ext: "xul", type: "application/vnd.mozilla.xul+xml"},
  { ext: "zip", type: "application/zip"},
  { ext: "3gp", type: "video/3gpp"},
  { ext: "3g2", type: "video/3gpp2"},
  { ext: "7z", type: "application/x-7z-compressed"}
];

function isPublicExist(path, dir=false) {
  try {
    var stat = fs.statSync(path);

    // check if directory
    if (stat.isDirectory() && !dir) {
      console.log("path exist but is directory");
      return false;
    }

    // try access the file
    fs.accessSync(path, fs.constants.F_OK | 'r');
    //console.log(`${path} exists`);

    return true;
  } catch (e) {
    //console.log(e);
    return false;
  }
}

function getFileHeader(path, stat) {
  var header = {}, found = false;
  var ext = path.split('/').pop().split('.').pop();
  //console.log(ext);

  for (var i = 0; i < TYPES.length; i++) {
    if (Array.isArray(TYPES[i].ext)) {
      for (var j = 0; j < TYPES[i].ext.length; j++) {
        if (TYPES[i].ext[j] == ext) {
          header['Content-Type'] = TYPES[i].type;
          header['Content-Length'] = stat.size;
          console.log(header);
          return header;
        }
      }
    } else {
      if (TYPES[i].ext == ext) {
        header['Content-Type'] = TYPES[i].type;
        header['Content-Length'] = stat.size;
        console.log(header);
        return header;
      }
    }
  }

  console.log("Unknwon file type", ext, path);
  console.log(header);
  return header;
}

function sendPublic(req, res, path) {
  try {
    console.log("Send Public", path);
    var stat = fs.statSync(path);
    res.writeHead(200, getFileHeader(path, stat));

    var readStream = fs.createReadStream(path);
    readStream.pipe(res);
    return true;
  } catch (e) {
    res.writeHead(400).write('Error reading resource');
    return true;
  }
}

/*
  Generator
*/

function generatePassword() {
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const special = "@!#()";
  var length = Math.floor(Math.random() * (18 - 12) + 12);

  var passwd = "";
  for (var i = 0; i < length; i++) {
    var random = Math.floor(Math.random() * (8 - 0) + 0);
    var selected = random >= 6 ? special : possible;
    passwd += selected.charAt(Math.floor(Math.random() * selected.length));
  }
  return (passwd);
}

/*
  Safe Assign
*/

function safeAssign(base, data, avoid=[]) {
  var result = true, skipped = [];
  var keys = Object.keys(data);

  for (var i = 0; i < keys.length; i++) {
    if (keys[i] == "_id") continue;
    if (!base.hasOwnProperty(keys[i])) continue; // dont assign if base don't have the key

    if (avoid.includes(keys[i])) {
      skipped.push(keys[i]);
    } else if (typeof data[keys[i]] != typeof base[keys[i]]) {
      skipped.push(keys[i]);
    } else if (typeof base[keys[i]] == "object" && !Array.isArray(base[keys[i]])) { // recursion
      var rec = safeAssign(base[keys[i]], data[keys[i]]);
      if (!rec.result) result = false;
      skipped.concat(rec.skipped);
      base[keys[i]] = rec.data;
    } else if (typeof base[keys[i]] == typeof data[keys[i]] && typeof base[keys[i]] != "object") {
      base[keys[i]] = data[keys[i]];
    } else {
      skipped.push(keys[i]);
    }
  }
  return ({result:result, data:base, skipped:skipped});
}

/*
  Requirement
*/
function checkRequired(needed, data) {
  if (data == undefined || data == null) data = {};
  var missing = [], keys = Object.keys(needed);

  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    // has key
    if (!data.hasOwnProperty(key)) {
      missing.push(key);
      continue;
    }
    // same type
    if (typeof needed[key] != typeof data[key]) {
      missing.push(key);
      continue;
    }
    // other
    if (key == "email") {
      if (!isEmail(data[key])) {
        missing.push(key);
        continue;
      }
    }
  }
  return ({
    result: missing.length == 0,
    missing: missing,
    info: missing.length == 0 ? "ok" : "invalid body, check missing/incorrect"
  });
}

function isEmail(mail) {
  return String(mail).toLowerCase().match(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
}

module.exports = {
  isPublicExist: isPublicExist,
  getFileHeader: getFileHeader,
  sendPublic: sendPublic,

  safeAssign: safeAssign,
  generatePassword: generatePassword,
  checkRequired: checkRequired,
  isEmail: isEmail
}

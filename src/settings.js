/*
** Zeta Network - ZCMS - Settings
** File: settings.js
** Author: Robot
** Version: .0.0
** USE STRICT REQUIRED
*/
"use strict";

var DB = require(__dirname + "/db.js"),
  ARG = require(__dirname + "/arg.js"),
  UPLOAD = require(__dirname + "/upload.js");

/*
  Init
*/

async function init() {
  var list = await DB.findAll(DB.settings);
  if (!list.result) {
    console.log("Fatal error loading settings");
    console.log(list);
    process.exit(20);
  }

  for (var i = 0; i < list.data.length; i++) {
    var s = list.data[i];
    module.exports[s.key] = s.value;
  }

  if (ARG.debug) console.log(module.exports);
}

/*
  Get
*/

async function get(req, res) {
  if (!req.body.hasOwnProperty('key')) return ({result:false, info:"Missing Key"});
  var ok = await getByKey(req.body.key);
  return (ok);
}

async function getAll(outside=false) {
  var list = await DB.findAll(DB.settings, outside);
  if (!list.result) return (list.result);
  return ({result:true, settings: list.data});
}

async function getByKey(key) {
  var set = await DB.findOne(DB.settings, {key:key});
  if (!set.result) return ({result:false, info:`Setting key "${key}" not found`});
  return ({result:true, setting: set.data});
}

/*
  Update
*/
async function update(body) {
  if (!body.hasOwnProperty('key') || !body.hasOwnProperty('value')) return ({result:false, info:"Missing key/value"});

  // get setting
  var s = await getByKey(body.key);
  if (!s.result) return s;

  // verify value
  var verif = verifyValue(s.setting, body.value);
  if (!verif.result) return ({result:false, info:"Invalid Value"});

  // verify specify
  var specify = await verifySpecify(s.setting, body.value);
  if (!specify.result) return (specify);

  s.setting.value = body.value;
  // update
  var update = await DB.updateOne(DB.settings, s.setting);
  if (!update.result) return (update);

  // run post update
  return await postUpdate(s.setting);
}

/*
  verifyValue
*/

function verifyValue(s, value) {
  if (s.type == "string" && typeof value == "string") return ({result:true, value:value});
  if (s.type == "number" && typeof value == "number") return ({result:true, value:value});
  if (s.type == "boolean" && typeof value == "boolean") return ({result:true, value:value});
  if (s.type == "password" && typeof value == "string") return ({result:true, value:value});

  if (s.type == "image") {
    if (!value.startsWith('/uploads')) return ({result:false, info:"Invalid value"});
    if (value.startsWith('/uploads/tmp')) {
      var imageOK = UPLOAD.validTempFile(value, `s_${s.key}`);
      console.log(imageOK);
      return imageOK.result == true ? {result:true, value: imageOK.name} : imageOK;
    }
    return ({result:true, value:value});
  }

  return ({result:false, info:"Invalid value"});
}

async function verifySpecify(s, value) {

  return ({result:true, value:value});
}

/*
  Post Update
*/
async function postUpdate(s) {
  // check if the setting change require something


  return ({result:true, info:"Setting updated", value:s.value});
}


/*
  MAP
*/
async function map(req, res) {
  switch (req.action[1]) {
    case "get": return await get(req, res);
    case "getAll": return await getAll(true);
    case "update": return await update(req.body);
    default: return ({result:false, info:`Not found under 'settings': ${req.action[1]}`, errorCode: 404});
  }
}

module.exports = {

  init: init,
  map: map

}

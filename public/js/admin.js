var UI = {};

document.addEventListener('DOMContentLoaded', () => {
  L.init();
  API.init();
  Breadcrumb.init();
  UI.sidenav = new Sidenav();
  UI.navbar = new Navbar();
  UI.viewManager = new ViewManager("dashboard");

  //Notification.init();
});

class ViewManager {

  constructor(startView=null) {
    this.container = document.querySelector('#main-container');
    //if (startView != null) this.setView(startView);
  }

  setView(id, data=null) {
    var view = this.getView(id, data);
    if (view == null) return alert("view ? "+ id);

    UI.viewId = id;
    UI.view = view;
    UI.view.draw();
  }

  update() {
    if (UI.view == null) return;
    UI.view.draw();
  }

  getView(id, data=null) {
    switch (id) {
      case "dashboard": return new DashboardView(this.container);
      case "apps": return new AppsView(this.container);
      default: return null;
    }
  }
}

/*
  Notification
*/
class Notification {

  static list = [];

  constructor(notif) {
    Object.assign(this, notif);
  }

  static init() {
    Notification.button = document.querySelector('#navbar [toggle="notification"] i');
    Notification.container = document.querySelector('#navbar #notif-container');
    Notification.button.addEventListener('click', (e) => { Notification.toggleShow(); });
    Notification.container.addEventListener('click', (e) => { Notification.toggleShow(); });
    Notification.refresh();
  }

  static toggleShow() {
    Notification.container.classList.toggle('show');
  }

  static async refresh() {
    var notif = await API.send('/notifications/getAll');
    if (!notif.result) return new Alert(false, notif.info);

    Notification.list = [];
    for (var i = 0; i < notif.notifications.length; i++) {
      Notification.list.push(new Notification(notif.notifications[i]));
    }
    Notification.draw();
  }

  // draw part

  static updateCounter(count) {
    var elem = document.querySelector('#notif-counter');
    if (count == 0) {
      elem.style.display = "none";
      return;
    }

    elem.innerHTML = count;
    elem.style.display = "block";
  }

  static draw() {
    var container = Notification.container;
    container.innerHTML = "";
    var unread = 0;

    if (Notification.list.length == 0) return container.appendChild(L.p(`<p class="center">$[[no_notifications]]</p>`));
    for (var i = 0; i < Notification.list.length; i++) {
      Notification.list[i].draw(container);
      if (Notification.list[i].read == false) unread++;
    }

    Notification.updateCounter(unread);
  }

  async setRead() {
    var ok = await API.send('/notifications/markAsRead', {id: this._id});
    if (!ok.result) return new Alert(false, ok.info);

    this.read = true;
    Notification.draw();
  }

  action(elem) {
    this.setRead();

    switch (this.event) {
      case "message": return UI.viewManager.setView("conversations", API.user.userId == this.message.from ? this.message.to : this.message.from);
      default: return new Alert(Alert.warning, `Event ? ${this.event}`);
    }
  }

  draw(container) {
    switch (this.event) {
      case "message":
        this.drawMessage(container);
        break;
      default:
        container.appendChild(`<div class="notif">Error: "${this.event}"</div>`);
        return;
    }

    container.querySelector(`[notif-id="${this._id}"]`).addEventListener('click', (e) => { this.action(); });
  }

  drawMessage(container) {
    container.appendChild(L.p(`
      <div class="notif row" notif-id="${this._id}">
        <img class="avatar" src="/avatar/${this.message.from}">
        <div class="coll">
          <p>${this.message.message}</p>
          <p class="date">${this.getDate(this.message.date)}</p>
        </div>
      </div>
    `));
  }

  getDate(date) {
    return "10m ago";
  }

}

class EventHandler {

  static handle(data) {
    console.log("HANDLE", data);
    if (!data.hasOwnProperty('event')) return null;

    switch (data.event) {
      case "message": return EventHandler.onMessage(data);
      case "recompile": return CompileAssistant.handle(data);
      default: new Alert(false, `Unknown event: ${data.event}`)
    }
  }

  static onMessage() {
    Notification.refresh();
    if (UI.viewId == "conversations") UI.view.draw();
  }

}

class Tools {
  constructor() {}

  static boolIcon(value=false) {
    var color = value == true ? "success" : "danger";
    var icon = value == true ? "fa-check" : "fa-xmark"
    return (`<i class="fa-solid ${icon} ${color}"></i>`);
  }
}

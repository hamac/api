document.addEventListener('DOMContentLoaded', () => {
  initUI();
  copyright();
});

function copyright() {
  var span = document.querySelector('footer #copyright-date');
  span.innerHTML = new Date().getFullYear();
  span.removeAttribute("id");
}

function initUI() {
  Dropdown.init();
  Collapse.init();
  Tooltip.init();
  ExpandCard.init();
}

/*
  Alert
*/

class Alert {

  static error = 0;
  static warning = 1;
  static info = 2;
  static success = 3;

  static min = 123456;
  static max = 987654;
  static timer = 1000 * 2.5;

  static fromBoolean(b) {
    return b === true ? Alert.success : Alert.error;
  }

  constructor(type, str) {
    this.type = typeof type == "boolean" ? Alert.fromBoolean(type) : type;
    this.str = str;
    this.id = Math.floor(Math.random() * (Alert.max - Alert.min) + Alert.min);
    this.build();
  }

  build() {
    this.verifyContainer();
    this.elem = this.buildElement();
    this.container.appendChild(this.elem);
    setTimeout(() => this.destroy(), Alert.timer);
  }

  buildElement() {
    var elem = document.createElement("div");
    elem.classList.add('alert', this.getClass());
    elem.setAttribute("id", `alert-${this.id}`);
    elem.innerHTML += L.pp(`
      <i class="${this.getIcon()}"></i>
      <p class="title">${this.getTitle()}</p>
      <p>${this.str}</p>
    `);
    return elem;
  }

  destroy() {
    //console.log("destroy", this.id);
    this.elem.remove();
  }

  verifyContainer() {
    var container = document.querySelector('#alert-container');
    if (container != null) {
      this.container = container;
      return;
    }

    // build container
    var elem = document.createElement("div");
    elem.setAttribute("id", "alert-container");
    document.querySelector('body').appendChild(elem);
    this.container = elem;
  }

  getTitle() {
    switch (this.type) {
      case Alert.error: return ("$[[error]]");
      case Alert.warning: return ("$[[warning]]");
      case Alert.info: return ("$[[info]]");
      case Alert.success: return ("$[[success]]");
      default: return "?";
    }
  }

  getClass() {
    switch (this.type) {
      case Alert.error: return "error";
      case Alert.warning: return "warning";
      case Alert.info: return "info";
      case Alert.success: return "success";
      default: return "";
    }
  }

  getIcon() {
    switch (this.type) {
      case Alert.error: return "fa-solid fa-do-not-enter";
      case Alert.warning: return "fa-solid fa-triangle-exclamation";
      case Alert.info: return "fa-solid fa-square-info";
      case Alert.success: return "fa-solid fa-check";
      default: return "";
    }
  }
}

/*
  ImageSwaper
*/

class ImageSwaper {

  constructor(elem, base, imgClass="") {
    this.uploaded = false;
    this.src = "";
    this.base = base;
    this.imgClass = imgClass;
    this.onUploaded = null;

    this.draw(elem);
  }

  draw(elem) {
    var url = this.src == "" ? this.base : this.src;
    elem.innerHTML = `
      <img class="${this.imgClass}" src="${url}">
      <form name="image-form" enctype="multipart/form-data" style="display: none;">
        <input type="file" name="image">
      </form>
    `;

    this.picture = elem.querySelector('img');
    this.input = elem.querySelector('input[type="file"]');
    this.form = elem.querySelector('form');
    this.input.addEventListener('change', (e) => { this.upload(); });
    this.picture.addEventListener('click', (e) => { this.choose(); });
  }

  update(elem, newLink=null) {
    if (newLink != null) this.src = newLink;
    this.draw(elem);
  }

  choose() {
    this.input.click();
  }

  get() {
    return this.uploaded == true ? this.src : "";
  }

  async upload() {
    this.uploaded = false;
    var fd = new FormData(this.form);
    var r = await API.send("/tmpUpload", fd);
    if (!r.result) return new Alert(false, r.info);

    this.uploaded = true;
    this.src = r.files.image;
    this.picture.src = this.src;
    if (this.onUploaded != null && typeof this.onUploaded == "function") this.onUploaded(r.files.image);
  }

}

/*
  Expandable Card
*/

class ExpandCard {

  static observer = null;
  static list = [];

  static init() {
    var list = document.querySelectorAll('.card.expand');
    for (var i = 0; i < list.length; i++) {
      ExpandCard.create(list[i]);
    }
    ExpandCard.register();
  }

  static create(elem) {
    for (var i = 0; i < ExpandCard.list.length; i++) {
      if (ExpandCard.list[i].elem == elem) return;
    }
    ExpandCard.list.push(new ExpandCard(elem));
  }

  static register() {
    if (ExpandCard.observer != null) ExpandCard.observer.disconnect()

    const config = { childList: true, subtree: true };
    ExpandCard.observer = new MutationObserver(ExpandCard.onMutation);
    ExpandCard.observer.observe(document.querySelector('body'), config);
  }

  static onMutation(list) {
    for (var i = 0; i < list.length; i++) {
      if (list[i].addedNodes.length == 0) continue;
      for (var j = 0; j < list[i].addedNodes.length; j++) {
        var actual = list[i].addedNodes[j];

        //console.log(i, j, typeof actual.querySelectorAll, actual.childNodes.length);
        if (actual.childNodes.length == 0) continue;
        if (!actual.classList.contains("card")) continue;
        if (!actual.classList.contains("expand")) continue;

        ExpandCard.create(actual);
      }
    }
  }

  constructor(elem) {
    if (elem == null || elem == undefined) return;
    this.elem = elem;
    this.destroyed = false;
    this.header = this.elem.querySelector('.header');
    this.content = this.elem.querySelector('.content');
    this.header.onclick = (e) => { this.toggleShow(); }
  }

  toggleShow() {
    if (this.destroyed == true) return;
    this.elem.classList.toggle("show");
  }

  destroy() {
    this.destroyed = true;
  }

}

/*
  Dropdown
*/

class Dropdown {

  static observer = null;
  static list = [];

  static init() {
    var list = document.querySelectorAll('.dropdown');
    for (var i = 0; i < list.length; i++) {
      Dropdown.create(list[i]);
    }
    Dropdown.register();
  }

  static create(elem) {
    for (var i = 0; i < Dropdown.list.length; i++) {
      if (Dropdown.list[i].elem == elem) return;
    }
    Dropdown.list.push(new Dropdown(elem));
  }

  static register() {
    if (Dropdown.observer != null) Dropdown.observer.disconnect()

    const config = { attributes: false, childList: true, subtree: true };
    Dropdown.observer = new MutationObserver(Dropdown.onMutation);
    Dropdown.observer.observe(document.querySelector('body'), config);
  }

  static onMutation(list) {
    for (var i = 0; i < list.length; i++) {
      if (list[i].addedNodes.length == 0) continue;
      for (var j = 0; j < list[i].addedNodes.length; j++) {

        var actual = list[i].addedNodes[j];

        //console.log(i, j, typeof actual.querySelectorAll, actual.childNodes.length);
        if (actual.childNodes.length == 0) continue;
        actual.querySelectorAll('.dropdown').forEach((item, x) => {
          //console.log("here", i, j, item);
          Dropdown.create(item);
        });

      }
    }
  }

  constructor(elem) {
    if (elem == null || elem == undefined) return;
    this.elem = elem;
    this.destroyed = false;
    this.type = elem.hasAttribute('select') ? "select" : "click";
    this.list = this.elem.querySelector('ul');
    this.main = this.elem.querySelector("a:not(dropdown-item)");
    this.main.onclick = () => this.toggleShow();
    this.elem.querySelectorAll("a.dropdown-item").forEach((item, i) => {
      item.addEventListener('click', (e) => { console.log(1); return this.swap(e.target); });
    });
  }

  toggleShow() {
    if (this.destroyed == true) return;
    this.elem.classList.toggle("show");
  }

  destroy() {
    this.destroyed = true;
  }

  swap(elem) {
    if (this.destroyed == true) return;

    if (this.type == "select") {
      // swap
      if (!elem.classList.contains('dropdown-item')) return elem.parentElement;
      var old = this.elem.querySelector("a:not(dropdown-item)");

      elem.remove();
      old.remove();

      elem.classList.remove('dropdown-item');
      this.elem.insertBefore(elem, this.list);
      this.main = this.elem.querySelector("a:not(dropdown-item)");
      this.main.onclick = () => this.toggleShow();

      old.classList.add('dropdown-item');
      this.list.appendChild(old);

      // swap content
      //elem.innerHTML = old;
      //this.main.innerHTML = chosen;
    }

    // hide
    this.toggleShow();
  }

}

/*
  Tooltip
*/

class Tooltip {

  static observer = null;
  static list = [];

  static init() {
    var list = document.querySelectorAll('[tooltip]');
    for (var i = 0; i < list.length; i++) {
      Tooltip.create(list[i]);
    }
    Tooltip.register();
  }

  static create(elem) {
    for (var i = 0; i < Tooltip.list.length; i++) {
      if (Tooltip.list[i].elem == elem) {
        Tooltip.list[i].destroy();
        Tooltip.list[i] = new Tooltip(elem);
        return;
      }
    }
    Tooltip.list.push(new Tooltip(elem));
  }

  static register() {
    if (Tooltip.observer != null) Tooltip.observer.disconnect()

    const config = { attributes: false, childList: true, subtree: true };
    Tooltip.observer = new MutationObserver(Tooltip.onMutation);
    Tooltip.observer.observe(document.querySelector('body'), config);
  }

  static onMutation(list) {
    for (var i = 0; i < list.length; i++) {
      if (list[i].addedNodes.length == 0) continue;
      for (var j = 0; j < list[i].addedNodes.length; j++) {

        var actual = list[i].addedNodes[j];

        //console.log(i, j, typeof actual.querySelectorAll, actual.childNodes.length);
        if (actual.childNodes.length == 0) continue;
        actual.querySelectorAll('[tooltip]').forEach((item, x) => {
          //console.log("here", i, j, item);
          Tooltip.create(item);
        });

      }
    }
  }

  constructor(elem) {
    if (elem == null || elem == undefined) return;
    if (!elem.hasAttribute('tooltip')) return;
    this.elem = elem;
    this.destroyed = false;
    this.text = elem.getAttribute('tooltip');
    this.tooltip = elem.querySelector('.tooltip');
    this.elem.addEventListener('mouseenter', (e) => { this.onEnter(); });
    this.elem.addEventListener('mouseleave', (e) => { this.onLeave(); });
  }

  destroy() {
    this.destroyed = true;
  }

  createTooltip() {
    var tooltip = document.createElement('p');
    tooltip.innerHTML = this.text;
    tooltip.classList.add('tooltip');
    this.elem.appendChild(tooltip);
    this.tooltip = tooltip;
  }

  onEnter() {
    if (this.destroyed == true) return;
    if (this.tooltip == null) this.createTooltip();

    this.tooltip.classList.add('show');
  }

  onLeave() {
    if (this.destroyed == true) return;

    this.tooltip.classList.remove('show');
  }

}

/*
  Modal
*/

class Modal {

  static min = 123456;
  static max = 987654;

  static verifyContainer() {
    var container = document.querySelector('#modal-container');
    if (container == null) {
      // build container
      container = document.createElement("div");
      container.setAttribute("id", "modal-container");
      document.querySelector('body').appendChild(container);
    }

    container.classList.remove('extended');
    container.classList.remove('hidden');
    return container;
  }

  static updateContainer() {
    var container = Modal.verifyContainer();
    if (container.childElementCount == 0) container.classList.add('hidden');
    else container.classList.remove('hidden');
  }

  constructor(config) {
    this.title = config.hasOwnProperty('title') ? config.title : "$[[no_title]]";
    this.content = config.content;
    this.blur = config.hasOwnProperty('blur') && config.blur == true ? "extended" : "";
    this.contentClass = config.hasOwnProperty('contentClass') ? config.contentClass : [];
    this.id = Math.floor(Math.random() * (Modal.max - Modal.min) + Modal.min);
    this.onclose = null;
    this.beforeClose = null;
    this.draw();
  }

  draw() {
    var container = Modal.verifyContainer();

    var c = typeof this.contentClass == "string" ? [this.contentClass].join(" ") : this.contentClass.join(" ");
    container.appendChild(L.p(`
      <div class="modal" modal-id="${this.id}">
        <div class="header">
          <h3>${this.title}</h3>
          <i class="fa-solid fa-xmark" action="close"></i>
        </div>
        <div class="content ${c}">${this.content}</div>
      </div>
    `));
    if (this.blur != "") container.classList.add('extended');
    this.elem = container.querySelector(`[modal-id="${this.id}"]`);

    this.elem.querySelector('[action="close"]').addEventListener('click', (e) => { this.destroy(); });
  }

  destroy() {
    if (typeof this.beforeClose == "function") this.beforeClose();
    this.elem.remove();
    Modal.updateContainer();
    if (typeof this.onclose == "function") this.onclose();
  }

}

/*
  Collapse
*/

class Collapse {

  static init() {
    var list = document.querySelectorAll('.collapse');
    for (var i = 0; i < list.length; i++) {
      new Collapse(list[i]);
    }
  }

  constructor(elem) {
    this.elem = elem;
    this.elem.addEventListener('click', () => this.toggleShow());
  }

  toggleShow() {
    this.elem.classList.toggle("show");
  }
}

/*
  Sidenav
*/

class Sidenav {
  constructor() {
    this.elem = document.querySelector("#sidenav");

    var list = this.elem.querySelectorAll('[toggle-view]');
    for (var i = 0; i < list.length; i++) {
      list[i].addEventListener('click', (e) => this.click(e.target));
    }
  }

  click(target) {
    if (!target.hasAttribute("toggle-view")) return this.click(target.parentElement);
    if (UI.viewManager == null) return;
    UI.viewManager.setView(target.getAttribute('toggle-view'));
  }
}

/*
  Navbar
*/

class Navbar {
  constructor() {
    this.elem = document.querySelector("#navbar");
    this.notifCounter = this.elem.querySelector("#notif-counter");
    document.querySelector('#avatar').src = API.user.avatar;
    this.elem.querySelector('[action="lock"]').addEventListener('click', (e) => { API.lock(true); });
    this.setNotification(0);
  }

  setNotification(nb) {
    var display = nb <= 0 ? "none" : "";
    this.notifCounter.style.display = display;
    this.notifCounter.innerHTML = nb;
  }
}

/*
  Breadcrumb
*/

class Breadcrumb {

  static elem = null;

  static init() {
    Breadcrumb.elem = document.querySelector('#breadcrumb');
    Breadcrumb.container = document.querySelector(".breadcrumb #actions-button");
  }

  static set(list) {
    Breadcrumb.button();
    Breadcrumb.elem.innerHTML = "";
    for (var i = 0; i < list.length; i++) {
      Breadcrumb.elem.appendChild(L.p(`<li toggle-view="${list[i].view}">$[[${list[i].label}]]</li>`));
    }
  }

  static button(list=null) {
    if (list == null) {
      Breadcrumb.container.innerHTML = "";
      return;
    }

    if (!Array.isArray(list)) list = [list];
    for (var i = 0; i < list.length; i++) {
      Breadcrumb.container.appendChild(list[i]);
    }
  }

}

/*
  Language
*/

class L {

  static reg = /[$][[][[]([a-zA-Z_0-9]+)[\]][\]]/gm;
  static data = {};

  static init() {
    var elem = document.querySelector('#lang-data');
    var json = JSON.parse(elem.innerText);
    L.data = json;
    elem.remove();
  }

  static pp(str) {
    var ids = [];
    for (var m = null; m = L.reg.exec(str); ids.push(m[1]));

    for (var i = 0; i < ids.length; i++) {
      var text = L.get(ids[i]);
      str = str.replace(`$[[${ids[i]}]]`, text);
    }
    return (str);
  }

  static p(str) {
    return L.toElement(L.pp(str));
  }

  static toElement(str) {
    var template = document.createElement('template');
    template.innerHTML = str.trim();
    return template.content.firstChild;
  }

  static get(key) {
    if (!L.data.hasOwnProperty(key)) return `[Language Error: ${key}]`;
    return L.data[key];
  }

}

// loading
function l(show=false) {
  document.querySelector('#loading').style.display = show ? "block" : "none";
}

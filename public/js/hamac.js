function goto(url, newTab=true) {
  if (!url.startsWith('http')) url = `${window.location.protocol}//${window.location.host}${url}`;
  console.log(url);
  document.querySelector('body').appendChild(L.p(`<a href="${url}" ${newTab == true ? 'target="_blank"' : ""} id="goto-link" style="display: none"></a>`));
  document.querySelector('#goto-link').click();
  document.querySelector('#goto-link').remove();
}

/*
  Form
*/

class Form {

  static emailRegexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  static telRegexp = /^\+((?:9[679]|8[035789]|6[789]|5[90]|42|3[578]|2[1-689])|9[0-58]|8[1246]|6[0-6]|5[1-8]|4[013-9]|3[0-469]|2[70]|7|1)(?:\W*\d){0,13}\d$/gm;

  static verify(obj, elem=null, verifyRequired=true, avoid=[], parent="") {
    if (elem == null) elem = document;
    var result = true, data = {};
    var keys = Object.keys(obj);

    for (var i = 0; i < keys.length; i++) {
      var namekey = parent + keys[i];
      console.log(i, namekey);
      if (avoid.includes(namekey)) continue;
      if (Array.isArray(obj[keys[i]])) continue;
      if (typeof obj[keys[i]] == "object" && !Array.isArray(obj[keys[i]])) {
        var rec = Form.verify(obj[keys[i]], elem, verifyRequired, avoid, parent+keys[i]+".");
        if (!rec.result) result = false;
        data[keys[i]] = rec.data;
        continue;
      }

      var selector = `[name="${parent}${keys[i]}"]`;
      var input = elem.querySelector(selector);
      var value = this.verifyInput(input, verifyRequired);

      if (value != null) {
        data[keys[i]] = value;
      } else {
        result = false;
      }
    }

    return ({result:result, data:data});
  }

  static verifyInput(input, verifyRequired) {
    var required = input.hasAttribute('required');
    if (verifyRequired === false) required = false;

    switch (input.type) {
      case "email":
        var validEmail = this.isEmail(input);
        if (!verifyRequired) return input.value;
        if (required & !validEmail) return null;
        return validEmail ? input.value : "";

      case "tel":
        var validTel = this.isTel(input);
        if (!verifyRequired) return input.value;
        if (required && !validTel) return null;
        return validTel ? input.value : "";

      case "password":
      case "text":
        if (!required) return input.value;
        var validText = (verifyRequired == false || (required && input.value.length > 0));
        this.setValid(input, validText);
        if (required && input.value.length == 0) return null;
        return input.value;

      case "number": {
        if (!required) return isNaN(input.valueAsNumber) ? 0 : input.valueAsNumber;
        var validNumber = !isNaN(input.valueAsNumber);
        this.setValid(input, validNumber);
        console.log("validNumber?", validNumber, input.valueAsNumber);
        return (validNumber == true ? input.valueAsNumber : -1);
      }

      case "select-multiple":
        return Form.selectMulitpleValue(input);

      case "select-one":
        return input.value;

      case "checkbox":
        return input.checked;

      default:
        new Alert(Alert.error, "Invalid input type: "+input.type);
        return null;
    }
  }

  static selectMulitpleValue(input) {
    var values = [];
    for (var i = 0; i < input.selectedOptions.length; i++) {
      values.push(input.selectedOptions[i].value);
    }
    return (values);
  }

  static setValid(input, status=false) {
    var parentClass = input.parentElement.classList;
    status ? parentClass.remove('is-invalid') : parentClass.add('is-invalid');
  }

  static isEmail(str) {
    var isElem = str instanceof HTMLElement;
    var valid = String(isElem ? str.value : str).toLowerCase().match(Form.emailRegexp);
    valid = valid != null;
    console.log("valid?email", valid);
    if (isElem) this.setValid(str, valid);
    return valid;
  }

  static isTel(str) {
    var isElem = str instanceof HTMLElement;
    var valid = String(isElem ? str.value : str).match(Form.telRegexp);
    valid = valid != null;
    console.log("valid?tel", valid);
    if (isElem) this.setValid(str, valid);
    return valid;
  }

}

/*
  API
*/

class API {

  static user = null;
  static ws = null;
  static auth = false;
  static callbacks = [];
  static disconnectBtn = null;
  static protocol = "marina-ws";

  static async send(action, data={}) {
    if (action.startsWith("/")) action = "/zcms"+action;
    if (API.ws != null && API.auth == true && !(data instanceof FormData)) return API.toWS(action, data);

    return new Promise((resolve, reject) => {
      var request = new XMLHttpRequest();
      request.open('POST', action, true);
      if (!(data instanceof FormData)) request.setRequestHeader("Content-Type", "application/json");
      if (API.user != null) {
        request.setRequestHeader('x-marina-email', API.user.email);
        request.setRequestHeader('x-marina-token', API.user.token);
        request.setRequestHeader('x-marina-secret', API.user.secret);
      }
      request.onload = function() {
        try {
          const res = request.responseText;
          resolve(JSON.parse(res));
        } catch (e) {
          resolve({result:false, info:"Error parsing body", error:e});
        }
      }
      request.send((data instanceof FormData) ? data : JSON.stringify(data));
    });
  }

  static async toWS(action, data={}) {
    if (!API.ws == null || !API.auth) {
      console.log("Not connected to ws");
      return ({result:false, info:"Websocket not connected"});
    }

    return new Promise((resolve, reject) => {
      const id = API.getId();
      API.callbacks.push({resolve:resolve, reject:reject, id:id});
      console.log("DATA", data);
      API.ws.send(JSON.stringify({action: action, tmpId: id, data:data}));
    });
  }

  static getId() {
    return Math.floor(Math.random() * (9876543210 - 1234567891) + 1234567891);
  }

  static init() {
    try {
      var elem = document.querySelector('#raw_user');
      API.user = JSON.parse(elem.innerHTML);
      elem.remove();
      API.connectWS();

      if (API.disconnectBtn != null) return;
      API.disconnectBtn = document.querySelector('#navbar [toggle="reconnect"]');
      API.disconnectBtn.addEventListener('click', API.tryReconnect);
    } catch (e) {
      console.log("Cannot load User");
      console.log(e);
    }
  }

  static getHost() {
    var host = window.location.host;
    if (host == null) host = window.location.hostname;
    return host;
  }

  static tryReconnect() {
    if (API.auth == true && API.ws != null) return new Alert(Alert.warning, "$[[already_connected]]");
    API.connectWS();
  }

  static connectWS() {
    const url = `ws://${API.getHost()}`;
    API.ws = new WebSocket(url, API.protocol);

    API.ws.onmessage = API.onMessage;
    API.ws.onclose = API.onClose;
    API.ws.onopen = (ev) => { /*console.log("onopen", ev);*/ };
  }

  static wsAuth() {
    if (API.ws == null) return;
    API.ws.send(`auth:${API.user.email}:${API.user.token}:${API.user.secret}`);
  }

  static onMessage(ev) {
    if (ev.data == "auth") return API.wsAuth();
    if (ev.data == "lock") return API.lock();
    if (ev.data == "authenticated") {
      new Alert(Alert.success, "$[[connected]]");
      API.auth = true;
      API.showDisconnect(false);
      return;
    }
    if (ev.data == "unlocked") {
      API.lockModal.destroy();
      new Alert(Alert.success, "$[[unlocked]]");
      API.locked = false;
      return;
    }
    if (ev.data == "unlock-failed") {
      new Alert(Alert.error, "$[[invalid_credential]]");
      return;
    }

    try {
      var body = JSON.parse(ev.data);
      if (body.hasOwnProperty('event')) return EventHandler.handle(body);
      if (body.hasOwnProperty('tmpId')) return API.resolve(body);
    } catch (e) {
      console.log(e);
      new Alert(Alert.error, e)
    }

    console.log("onmessage", ev);
  }

  static resolve(body) {
    for (var i = 0; i < API.callbacks.length; i++) {
      if (API.callbacks[i].id != body.tmpId) continue;
      API.callbacks[i].resolve(body);
      API.callbacks.splice(i, 1);
      return;
    }
  }

  static onClose(ev) {
    var reason = ev.reason;

    if (ev.code == 1006) reason = "Abnormal Closure (Server close it)";
    new Alert(Alert.warning, `Disconnected: ${reason}`);

    console.log("onclose", ev);
    API.auth = false;
    API.ws = null;
    API.showDisconnect(true);
  }

  static showDisconnect(show) {
    if (typeof show != "boolean") show = true;
    if (API.disconnectBtn == null) return new Alert(Alert.warning, "Disconnect Button Null");
    API.disconnectBtn.style.display = show == true ? "block" : "none";
  }

  static lock(send=false) {
    if (API.locked) return; // check already locked

    if (send) API.ws.send('lock');
    const content = `
      <img class="avatar big" src="${this.user.avatar}">
      <h3>${this.user.information.lastName} ${this.user.information.firstName}</h3>
      <div class="input-group">
        <i class="fa-solid fa-key-skeleton"></i>
        <input type="password" name="password" placeholder="$[[password]]">
      </div>
      <button class="primary mh-auto" style="display: inline-block" action="unlock">$[[unlock]]</button>
    `;
    API.lockModal = new Modal({title:"$[[locked]]", content: content, contentClass: "center", blur: true});
    API.lockModal.elem.querySelector('button[action="unlock"]').addEventListener('click', API.tryUnlock);
    API.locked = true;
  }

  static tryUnlock() {
    if (API.lockModal == null) return console.log("Internal Error: Cannot unlock without the lock modal");

    var input = API.lockModal.elem.querySelector('input[type="password"]');

    if (input.value == "") {
      input.parentElement.classList.add('is-invalid');
      return;
    }

    input.parentElement.classList.remove('is-invalid');
    API.ws.send(`unlock:${input.value}`);
  }

}
